package main_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	iqs "git.drupalcode.org/project/insta_queue_scheduler"
)

func TestGetQueues(t *testing.T) {
  t.Parallel()

	qm := createQueueManager(8, nil, []string{"queue_one"}, nil)

	assert.Len(t, qm.GetQueues(), 0)

	qm.PopulateQueues(false)

	assert.Len(t, qm.GetQueues(), 1)
}

func TestCanManageQueue(t *testing.T) {
  t.Parallel()

	qm := createQueueManager(8, nil, []string{"queue_one"}, nil)
	qm.PopulateQueues(false)

	assert.True(t, qm.CanManageQueue("queue_one"))
	assert.False(t, qm.CanManageQueue("queue_two"))
}

func createQueueManager(queueItems int, queueItemsErr error, queues []string, queuesError error) *iqs.QueueManager {
	db := &MockDatabase{
		GetProcessableQueueItemCountFunc: func(queue string) (int, error) {
			return queueItems, queueItemsErr
		},
	}

	eb := iqs.NewEventBus()

	drush := &MockDrushCommands{
		GetInstaQueuesFunc: func() ([]string, error) {
			return queues, queuesError
		},
	}

	return iqs.NewQueueManager(db, eb, drush)
}
