package main

import (
	"os"
	"slices"
	"sync"

	_ "github.com/go-sql-driver/mysql"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

func setup(config *Config) {
	var logLevel zerolog.Level

	switch config.LogLevel {
	case "trace":
		logLevel = zerolog.TraceLevel
	case "debug":
		logLevel = zerolog.DebugLevel
	default:
		logLevel = zerolog.InfoLevel
	}

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.SetGlobalLevel(logLevel)
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	log.Info().Str("module", "app").Msg("Starting insta queue scheduler")
	log.Debug().Str("module", "app").Msg("Starting setup")
}

func main() {
	config := NewConfig()

	setup(config)

	// Create instances.
	eventBus := NewEventBus()

	database := NewDatabase("mysql", config.MysqlDsn)
	defer database.Close()

	drush := NewDrushCommands(config.DrushBaseDir, config.DrushCommand, config.DrushViaDdev)
	queueManager := NewQueueManager(database, eventBus, drush)
	workerManager := NewWorkerManager(drush, queueManager)
	server := NewServer(config.SocketPath, workerManager, eventBus)
	scheduler := NewScheduler(config.SchedulerInterval, database, queueManager, workerManager, eventBus)

	// Get queues.
	queueManager.PopulateQueues(false)

	// Handle reload.
	eventBus.Subscribe(EVENT_RELOAD, func() {
		oldQueues := queueManager.GetQueues()
		queueManager.PopulateQueues(false)
		newQueues := queueManager.GetQueues()

		for _, oldQueue := range oldQueues {
			if !slices.Contains(newQueues, oldQueue) {
				workerManager.RemoveWorker(oldQueue)
			}
		}

    eventBus.Publish(EVENT_RESTART_SCHEDULER)
	})

	// Run threads.
	var wg sync.WaitGroup
	wg.Add(2)

	// Run server.
	go func() {
		server.Listen()
		wg.Done()
	}()

	// Run scheduler.
	go func() {
		scheduler.Run()
		wg.Done()
	}()

	// Block main, until exit.
	go eventBus.PublishShutdownOnSignal()
	wg.Wait()
}
