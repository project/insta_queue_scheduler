package main

import (
	"sync"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type Scheduler struct {
	db            *Database
	queueManager  *QueueManager
	workerManager *WorkerManager
	eventBus      *EventBus
	log           zerolog.Logger
	interval      time.Duration
}

func NewScheduler(interval time.Duration, db *Database, queueManager *QueueManager, workerManager *WorkerManager, eventBus *EventBus) *Scheduler {
	return &Scheduler{
		db:            db,
		queueManager:  queueManager,
		workerManager: workerManager,
		eventBus:      eventBus,
		log:           log.With().Str("module", "scheduler").Logger(),
		interval:      interval,
	}
}

func (s *Scheduler) Run() {
	ticker := time.NewTicker(s.interval)
	stop := make(chan int)
	run := make(chan int)

	s.eventBus.Subscribe(EVENT_SHUTDOWN, func() {
		s.log.Info().Msg("Stopping scheduler")

		stop <- 0
	})

	s.eventBus.Subscribe(EVENT_RESTART_SCHEDULER, func() {
		s.log.Info().Msg("Restarting scheduler")

		run <- 0
	})

	// Initial run.
	s.checkQueues()

	for {
		select {
		case <-ticker.C:
			s.checkQueues()
		case <-run:
			s.checkQueues()
		case <-stop:
			ticker.Stop()
			return
		}
	}
}

func (s *Scheduler) checkQueues() {
	queues := s.queueManager.GetQueues()

	s.log.Debug().Msg("Processing queues if necessary")

	var wg sync.WaitGroup
	wg.Add(len(queues))

	for _, queue := range queues {
		go func(queue string) {
			s.workerManager.RunWorker(queue, false)
			wg.Done()
		}(queue)
	}

	wg.Wait()
}
