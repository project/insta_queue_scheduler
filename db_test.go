package main_test

import (
	"database/sql"
	"fmt"
	"os"
	"testing"

	_ "modernc.org/sqlite"
	"github.com/stretchr/testify/assert"

	iqs "git.drupalcode.org/project/insta_queue_scheduler"
)

const DSN = "file:test.db?cache=shared&mode=memory"

func TestMain(m *testing.M) {
	db, err := sql.Open("sqlite", DSN)
	if err != nil {
		panic("Could not setup db")
	}

	statements := []string{
		"CREATE TABLE IF NOT EXISTS queue ( item_id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(255) NOT NULL, data LONGBLOB DEFAULT NULL, expire INTEGER NOT NULL DEFAULT 0, created INTEGER NOT NULL DEFAULT 0 );",

		"INSERT INTO queue (name, expire, created) VALUES ('queue_one', 0, 1700060603);",
		"INSERT INTO queue (name, expire, created) VALUES ('queue_one', 0, 1700060603);",
		"INSERT INTO queue (name, expire, created) VALUES ('queue_one', 0, 1700060603);",
		"INSERT INTO queue (name, expire, created) VALUES ('queue_one', 0, 1700060603);",
		"INSERT INTO queue (name, expire, created) VALUES ('queue_one', 0, 1700060603);",
		"INSERT INTO queue (name, expire, created) VALUES ('queue_one', 0, 1700060603);",
		"INSERT INTO queue (name, expire, created) VALUES ('queue_one', 0, 1700060603);",
		"INSERT INTO queue (name, expire, created) VALUES ('queue_one', 0, 1700060603);",
		"INSERT INTO queue (name, expire, created) VALUES ('queue_one', 0, 1700060603);",
		"INSERT INTO queue (name, expire, created) VALUES ('queue_one', 0, 1700060603);",

		"INSERT INTO queue (name, expire, created) VALUES ('queue_two', 0, 1700060603);",
		"INSERT INTO queue (name, expire, created) VALUES ('queue_two', 0, 1700060603);",
		"INSERT INTO queue (name, expire, created) VALUES ('queue_two', 0, 1700060603);",

		"INSERT INTO queue (name, expire, created) VALUES ('queue_one', 1700060603, 1700060603);",
		"INSERT INTO queue (name, expire, created) VALUES ('queue_one', 1700060603, 1700060603);",
		"INSERT INTO queue (name, expire, created) VALUES ('queue_one', 1700060603, 1700060603);",
	}

	for _, stmnt := range statements {
		_, err := db.Exec(stmnt)
		if err != nil {
			fmt.Println(err)
			panic("Could not exec db query")
		}
	}

	os.Exit(m.Run())
}

func TestDatabaseConnection(t *testing.T) {
	database := iqs.NewDatabase("sqlite", DSN)
	defer database.Close()
}

func TestGetProcessableQueueItemCount(t *testing.T) {
	database := iqs.NewDatabase("sqlite", DSN)
	defer database.Close()

	count, err := database.GetProcessableQueueItemCount("queue_one")
	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, 10, count)
}

type MockDatabase struct {
	CloseFunc                        func() error
	GetProcessableQueueItemCountFunc func(queue string) (int, error)
}

func (md *MockDatabase) Close() error {
	if md.CloseFunc != nil {
		return md.CloseFunc()
	}

	return nil
}

func (md *MockDatabase) GetProcessableQueueItemCount(queue string) (int, error) {
	if md.GetProcessableQueueItemCountFunc != nil {
		return md.GetProcessableQueueItemCountFunc(queue)
	}
	return 0, nil
}
