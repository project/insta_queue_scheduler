package main

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type WorkerResult int

const (
	// Status from drush exit code.
	WORKER_AVAILABLE_ITEMS_PROCESSED WorkerResult = 0
	WORKER_ALREADY_RUNNING           WorkerResult = 10
	WORKER_TIME_LIMIT_EXCEEDED       WorkerResult = 11
	WORKER_QUEUE_SUSPENDED           WorkerResult = 12
	// Internal status.
	WORKER_QUEUE_NO_WORK WorkerResult = 21
)

type Worker struct {
	active bool
	queue  string
	drush  *DrushCommands
	log    zerolog.Logger
}

// Create new worker.
func NewWorker(queue string, drush *DrushCommands) *Worker {
	return &Worker{
		active: false,
		queue:  queue,
		drush:  drush,
		log:    log.With().Str("module", "worker").Str("queue", queue).Logger(),
	}
}

// Run worker.
func (w *Worker) Run() (WorkerResult, error) {
	w.active = true

	w.log.Info().Msg("Worker start")

	exitCode, err := w.drush.ProcessQueue(w.queue)
	if err != nil {
		w.active = false

		w.log.Err(err).Msg("Worker could not start")

		return 0, err
	}

	w.active = false

	w.log.Info().Msg("Worker done")

	return WorkerResult(exitCode), nil
}
