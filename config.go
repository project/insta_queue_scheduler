package main

import (
	"time"

	env "github.com/Netflix/go-env"
	"github.com/rs/zerolog/log"
)

type Config struct {
	MysqlDsn          string        `env:"IQS_SQL_DSN,required=true"`
	DrushCommand      string        `env:"IQS_DRUSH_COMMAND,default=drush"`
	DrushBaseDir      string        `env:"IQS_DRUSH_BASE_DIR,required=true"`
	SocketPath        string        `env:"IQS_SOCKET_PATH,required=true"`
	SchedulerInterval time.Duration `env:"IQS_SCHEDULER_INTERVAL,default=60s"`
	DrushViaDdev      bool          `env:"IQS_DRUSH_VIA_DDEV,default=false"`
	LogLevel          string        `env:"IQS_LOG_LEVEL,default=info"`
}

func NewConfig() *Config {
	var config Config

	_, err := env.UnmarshalFromEnviron(&config)
	if err != nil {
		log.Fatal().Err(err).Msg("Could not load config from environment")
	}

	return &config
}
