package main

import (
	"bufio"
	"fmt"
	"net"
	"strings"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type Server struct {
	socketPath    string
	workerManager *WorkerManager
	eventBus      *EventBus
	log           zerolog.Logger
}

// Create new server.
func NewServer(socketPath string, workerManager *WorkerManager, eventBus *EventBus) *Server {
	return &Server{
		socketPath:    socketPath,
		workerManager: workerManager,
		eventBus:      eventBus,
		log:           log.With().Str("module", "server").Logger(),
	}
}

// Start server and listen for connections.
func (s *Server) Listen() {
	l, err := net.Listen("unix", s.socketPath)
	if err != nil {
		s.log.Fatal().Err(err).Msg("Could not start server")
	}
	defer l.Close()

	s.log.Info().Msgf("Server listening for connections on: %s", s.socketPath)

  // Close on shutdown.
  s.eventBus.Subscribe(EVENT_SHUTDOWN, func() {
    l.Close()
  })

	for {
		conn, err := l.Accept()
		if err != nil {
			// Check if the error is due to the listener being closed
			if opErr, ok := err.(*net.OpError); ok && opErr.Err.Error() == "use of closed network connection" {
				s.log.Info().Msg("Stopping server: listener closed")
				return
			}

			s.log.Err(err).Msg("Could not accept connection")
			continue
		}

		go s.handleConnection(conn)
	}
}

// Handle a connection.
func (s *Server) handleConnection(conn net.Conn) {
	defer func() {
		s.log.Debug().Msg("Connection closed")

		conn.Close()
	}()

	s.log.Debug().Msg("New connection")

	reader := bufio.NewReader(conn)

	for {
		message, err := reader.ReadString('\n')
		if err != nil {
			if err.Error() == "EOF" {
				return
			}

			fmt.Println("Error reading:", err)
			s.log.Err(err).Msg("Could not read message")
			return
		}

		message = strings.TrimRight(message, string('\n'))
		s.log.Debug().Str("request_message", message).Msg("Received message")

		go s.handleMessage(message)
	}
}

// Handle a message.
func (s *Server) handleMessage(message string) {
	if strings.HasPrefix(message, "item_created:") {
		parts := strings.Split(message, ":")
		queue := parts[1]

		s.workerManager.RunWorker(queue, true)

    return;
	}

  if (message == "reload") {
    s.eventBus.Publish(EVENT_RELOAD)

    return;
  }

  s.log.Warn().Msgf("Received unknown message: %s", message)
}
