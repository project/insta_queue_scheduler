package main

import (
	"bytes"
	"encoding/json"
	"os/exec"
	"strings"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type DrushCommandResult struct {
	exitCode int
	stdout   bytes.Buffer
}

type DrushCommandsInterface interface {
	GetInstaQueues() ([]string, error)
	ProcessQueue(queueName string) (int, error)
}

type DrushCommands struct {
	dir     string
	ddev    bool
	command string
	log     zerolog.Logger
}

func NewDrushCommands(dir string, command string, ddev bool) *DrushCommands {
	return &DrushCommands{
		dir:     dir,
		ddev:    ddev,
		command: command,
		log:     log.With().Str("module", "drush").Logger(),
	}
}

func (dc *DrushCommands) run(arg ...string) (DrushCommandResult, error) {
	var result DrushCommandResult
	start := time.Now()

	if dc.ddev {
		arg = append([]string{"drush"}, arg...)
	}

	dc.log.Trace().Msgf("Running command: '%s %s'", dc.command, strings.Join(arg, " "))

	cmd := exec.Command(dc.command, arg...)
	cmd.Dir = dc.dir
	cmd.Stdout = &result.stdout

	err := cmd.Start()
	if err != nil {
		dc.log.Debug().Err(err).Msg("Could not run drush command")

		return result, err
	}

	cmd.Wait()

	result.exitCode = cmd.ProcessState.ExitCode()

	dc.log.Trace().
		Int("code", result.exitCode).
		Str("exec_time", time.Now().Sub(start).String()).
		Msgf("Ran command: '%s %s'", dc.command, strings.Join(arg, " "))

	return result, nil
}

func (dc *DrushCommands) GetInstaQueues() ([]string, error) {
	result, err := dc.run("insta_queue:get-insta-queues")
	if err != nil {
		return nil, err
	}

  bytes := result.stdout.Bytes()

	var queues []string
	if err = json.Unmarshal(bytes, &queues); err != nil {
    dc.log.Trace().
      Err(err).
      Str("output", string(bytes)).
      Msgf("Error unmarshalling queue data")

		return nil, err
	}

	return queues, nil
}

func (dc *DrushCommands) ProcessQueue(queueName string) (int, error) {
	result, err := dc.run("insta_queue:process-queue", queueName)
	if err != nil {
		return 0, err
	}

	return result.exitCode, nil
}
