package main

import (
	"slices"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type QueueManager struct {
	queues   []string
	db       DatabaseInterface
	eventBus *EventBus
	drush    DrushCommandsInterface
	log      zerolog.Logger
}

// Create new queue manager instance.
func NewQueueManager(db DatabaseInterface, eventBus *EventBus, drush DrushCommandsInterface) *QueueManager {
	return &QueueManager{
		db:       db,
		eventBus: eventBus,
		drush:    drush,
		log:      log.With().Str("module", "queue_manager").Logger(),
	}
}

// Get queues.
func (qm *QueueManager) GetQueues() []string {
	return qm.queues
}

// Checks whether given queue can be managed.
func (qm *QueueManager) CanManageQueue(name string) bool {
	return slices.Contains(qm.queues, name)
}

func (qm *QueueManager) QueueHasWork(name string) bool {
	if !qm.CanManageQueue(name) {
		return false
	}

	count, err := qm.db.GetProcessableQueueItemCount(name)
	if err != nil {
		qm.log.Err(err).Msg("Could not get processable queue item count")
		return false
	}

	return count > 0
}

func (qm *QueueManager) PopulateQueues(allowFatal bool) {
	qm.log.Info().Msg("Getting processable queues from drupal")

	queues, err := qm.drush.GetInstaQueues()
	if err != nil {
		if allowFatal {
			qm.log.Fatal().Err(err).Msg("Could not get queues from drupal")
		}

		qm.log.Err(err).Msg("Could not get queues from drupal")
	}

	qm.queues = queues
}
