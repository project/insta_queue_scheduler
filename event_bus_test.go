package main_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	iqs "git.drupalcode.org/project/insta_queue_scheduler"
)

func TestEventBus(t *testing.T) {
	eb := iqs.NewEventBus()

	working := false
	ctx, _ := context.WithTimeout(context.Background(), time.Second*1)

	s := make(chan bool, 1)

	eb.Subscribe("test", func() {
		working = true
		s <- true
	})

	eb.Publish("test")

	select {
	case <-s:
		break
	case <-ctx.Done():
		break
	}

	assert.True(t, working)
}
