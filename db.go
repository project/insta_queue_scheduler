package main

import (
	"database/sql"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type QueueItem struct {
	itemId  string
	expire  int64
	created int64
}

type DatabaseInterface interface {
  Close() error
  GetProcessableQueueItemCount(queue string) (int, error)
}

type Database struct {
	db  *sql.DB
	log zerolog.Logger
}

// Create new database.
func NewDatabase(driver string, dsn string) *Database {
	log := log.With().Str("module", "database").Logger()

	db, err := sql.Open(driver, dsn)
	if err != nil {
		log.Fatal().Err(err).Msg("Could not open sql connection to database")
	}

	return &Database{
		db:  db,
		log: log,
	}
}

// Close db connection.
func (d *Database) Close() error {
	d.log.Debug().Msg("Closing database connection")

	return d.db.Close()
}

func (d *Database) GetProcessableQueueItemCount(queue string) (int, error) {
	var count int
	err := d.runQueryRow("SELECT COUNT(*) FROM queue WHERE name=? AND expire=0", queue).Scan(&count)
	if err != nil {
		d.log.Err(err).Msg("Error running sql query to get processable queue item count")

		return 0, err
	}

	return count, nil
}

func (d *Database) runQuery(query string, args ...any) (*sql.Rows, error) {
	start := time.Now()
	d.log.Trace().Msgf("Executing sql query: '%s'", query)

	rows, err := d.db.Query(query, args...)
	if err != nil {
		d.log.Debug().Err(err).Msg("Could not execute sql query")

		return rows, err
	}

	d.log.Trace().
		Str("exec_time", time.Now().Sub(start).String()).
		Msgf("Executed sql query: '%s'", query)

	return rows, err
}

func (d *Database) runQueryRow(query string, args ...any) *sql.Row {
	start := time.Now()
	d.log.Trace().Msgf("Executing sql query: '%s'", query)

	row := d.db.QueryRow(query, args...)

	d.log.Trace().
		Str("exec_time", time.Now().Sub(start).String()).
		Msgf("Executed sql query: '%s'", query)

	return row
}
