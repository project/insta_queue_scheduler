package main

import (
	"fmt"
	"sync"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type WorkerManager struct {
	workerMutex  sync.Mutex
	workers      map[string]*Worker
	staleWorkers []string
	drush        *DrushCommands
	queueManager *QueueManager
	log          zerolog.Logger
}

func NewWorkerManager(drush *DrushCommands, queueManager *QueueManager) *WorkerManager {
	return &WorkerManager{
		workerMutex:  sync.Mutex{},
		workers:      make(map[string]*Worker),
		drush:        drush,
		queueManager: queueManager,
		log:          log.With().Str("module", "worker_manager").Logger(),
	}
}

func (wm *WorkerManager) getWorker(queue string) *Worker {
	wm.workerMutex.Lock()
	defer wm.workerMutex.Unlock()

	// Worker already exists.
	if worker, ok := wm.workers[queue]; ok {
		return worker
	}

	log.Info().Str("queue", queue).Msg("Creating new worker")

	worker := NewWorker(queue, wm.drush)
	wm.workers[queue] = worker

	return worker
}

func (wm *WorkerManager) RunWorker(queue string, force bool) (WorkerResult, error) {
	log := wm.log.With().Str("queue", queue).Logger()

	wm.garbageCollect()

	if !wm.queueManager.CanManageQueue(queue) {
		err := fmt.Errorf("Queue '%s' cannot is not an insta queue", queue)
		log.Err(err)

		return 0, err
	}

	log.Debug().Msg("Request to run worker")

	// Check if there are actual items to process.
	if !force && !wm.queueManager.QueueHasWork(queue) {
		log.Debug().Msg("Not running worker: queue has no items to process")

		return WORKER_QUEUE_NO_WORK, nil
	}

	worker := wm.getWorker(queue)

	// Worker already running.
	if worker.active {
		log.Debug().Msg("Not running worker: another worker is already running")

		return WORKER_ALREADY_RUNNING, nil
	}

	log.Info().Msg("Running worker")

	res, err := worker.Run()
	if err != nil {
		log.Err(err).Msg("Failed to run worker")

		return 0, err
	}

	// Re-run worker if time limit was exceeded or if all items have been processed.
	// This is to ensure a consistent flow, when there are still items
	// in the queue to be processed, or if new items where added while processing.
	if res == WORKER_TIME_LIMIT_EXCEEDED || res == WORKER_AVAILABLE_ITEMS_PROCESSED {
		log.Debug().Msg("Requesting re-run of worker due to exit code")
		return wm.RunWorker(queue, false)
	}

	return res, nil
}

func (wm *WorkerManager) RemoveWorker(queue string) bool {
	wm.workerMutex.Lock()
	defer wm.workerMutex.Unlock()

	worker, ok := wm.workers[queue]
	// Worker does not exist.
	if !ok {
		return true
	}

	// Worker is idle, so delete it from the map.
	if !worker.active {
		wm.log.Info().Str("queue", queue).Msg("Removing stale worker")

		delete(wm.workers, queue)

		return true
	}

	// Otherwise, we add the worker to stale workers.
	wm.staleWorkers = append(wm.staleWorkers, queue)

	return false
}

func (wm *WorkerManager) garbageCollect() {
	var staleWorkers []string

	for _, queue := range wm.staleWorkers {
		if ok := wm.RemoveWorker(queue); !ok {
			staleWorkers = append(staleWorkers, queue)
		}
	}

	wm.staleWorkers = staleWorkers
}
