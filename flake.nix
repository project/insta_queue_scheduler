{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    ww-utils.url = "github:wunderwerkio/nix-ww-utils";
  };

  outputs = inputs @ {
    self,
    nixpkgs,
    ww-utils,
  }: {
    packages = ww-utils.lib.forEachWunderwerkSystem (
      system: let
        pkgs = nixpkgs.legacyPackages.${system};

        instaQueueScheduler = import ./nix/package.nix {
          inherit pkgs;
        };
        instaQueueScheduler-cross-aarch64-linux = import ./nix/package.nix {
          pkgs = pkgs.pkgsCross.aarch64-multiplatform;
        };
      in {
        inherit instaQueueScheduler;
        inherit instaQueueScheduler-cross-aarch64-linux;
        default = instaQueueScheduler;
      }
    );

    defaultPackage = ww-utils.lib.forEachWunderwerkSystem (
      system: self.packages.${system}.default
    );

    devShells = ww-utils.lib.forEachWunderwerkSystem (
      system: let
        pkgs = import nixpkgs {
          inherit system;
        };
      in rec {
        default = pkgs.mkShell {
          buildInputs = with pkgs; [
            go
          ];
        };
      }
    );

    formatter = ww-utils.lib.forEachWunderwerkSystem (
      system:
        nixpkgs.legacyPackages.${system}.alejandra
    );

    nixosModules.default = import ./nix/module.nix inputs;

    nixConfig = {
      extra-substituters = ["https://insta-queue-scheduler.cachix.org"];
      extra-trusted-public-keys = ["insta-queue-scheduler.cachix.org-1:Lrg+QgfWwPAMgsR62F+k/lTeM9axfR6SC1OTsA75C4I="];
    };
  };
}
