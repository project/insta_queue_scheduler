package main

import (
	"os"
	"os/signal"
	"sync"
	"sync/atomic"
	"syscall"
)

type Event string

const (
	EVENT_SHUTDOWN          Event = "event_sutdown"
	EVENT_RELOAD            Event = "event_reload"
	EVENT_RESTART_SCHEDULER Event = "event_restart_scheduler"
)

// CallbackFunc represents a callback function for handling events.
type CallbackFunc func()

// EventBus represents an event bus.
type EventBus struct {
	subscribers map[Event]map[int]CallbackFunc
	mu          sync.Mutex
	counter     int32
}

// Create new event bus.
func NewEventBus() *EventBus {
	return &EventBus{
		subscribers: make(map[Event]map[int]CallbackFunc),
	}
}

// Subscribe subscribes a callback function to a specific event.
func (eb *EventBus) Subscribe(event Event, callback CallbackFunc) int {
	eb.mu.Lock()
	defer eb.mu.Unlock()

	if eb.subscribers == nil {
		eb.subscribers = make(map[Event]map[int]CallbackFunc)
	}

	if _, ok := eb.subscribers[event]; !ok {
		eb.subscribers[event] = make(map[int]CallbackFunc)
	}

	id := int(atomic.AddInt32(&eb.counter, 1))
	eb.subscribers[event][id] = callback
	return id
}

// Unsubscribe unsubscribes a callback function from a specific event.
func (eb *EventBus) Unsubscribe(event Event, id int) {
	eb.mu.Lock()
	defer eb.mu.Unlock()

	if subscribers, ok := eb.subscribers[event]; ok {
		delete(subscribers, id)
	}
}

// Publish publishes an event to all subscribers.
func (eb *EventBus) Publish(event Event) {
	eb.mu.Lock()
	defer eb.mu.Unlock()

	if subscribers, ok := eb.subscribers[event]; ok {
		for _, callback := range subscribers {
			// Invoke the callback function (non-blocking)
			go func(callback CallbackFunc) {
				callback()
			}(callback)
		}
	}
}

// Publish EVENT_SHUTDOWN on interrupt.
func (eb *EventBus) PublishShutdownOnSignal() {
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc, os.Interrupt, syscall.SIGTERM)

	<-sigc

	eb.Publish(EVENT_SHUTDOWN)
}
