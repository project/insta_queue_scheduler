package main_test

type MockDrushCommands struct {
	GetInstaQueuesFunc func() ([]string, error)
	ProcessQueueFunc   func(queueName string) (int, error)
}

func (mdc *MockDrushCommands) GetInstaQueues() ([]string, error) {
	if mdc.GetInstaQueuesFunc != nil {
		return mdc.GetInstaQueuesFunc()
	}

	return nil, nil
}

func (mdc *MockDrushCommands) ProcessQueue(queueName string) (int, error) {
	if mdc.GetInstaQueuesFunc != nil {
		return mdc.ProcessQueueFunc(queueName)
	}

	return 0, nil
}
