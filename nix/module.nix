inputs: {
  lib,
  pkgs,
  config,
  ...
}: with lib; let
  cfg = config.services.instaQueueScheduler;
  inherit (pkgs.stdenv.hostPlatform) system;
in {
  options.services.instaQueueScheduler = {
    enable = mkEnableOption (mdDoc "Drupal Insta Queue Scheduler");

    package = mkPackageOption inputs.self.packages.${system} "instaQueueScheduler" {};

    phpCliPackage = mkPackageOption pkgs "php" {};

    user = mkOption {
      default = "iqs";
      type = types.str;
      description = lib.mdDoc ''
        User account under which instaQueueScheduler runs.

        ::: {.note}
        If left as the default value this user will automatically be created
        on system activation, otherwise you are responsible for
        ensuring the user exists before the insta-queue-scheduler service starts.
        :::
      '';
    };

    group = mkOption {
      default = "iqs";
      type = types.str;
      description = lib.mdDoc ''
        Group account under which instaQueueScheduler runs.

        ::: {.note}
        If left as the default value this user will automatically be created
        on system activation, otherwise you are responsible for
        ensuring the user exists before the insta-queue-scheduler service starts.
        :::
      '';
    };

    sqlDsn = mkOption {
      type = types.str;
      default = "";
      description = lib.mdDoc ''
        Connection DSN to the database.
      '';
      example = "user:pass@tcp(localhost:3306)/database";
    };

    drushBaseDir = mkOption {
      type = types.str;
      default = "/var/www/html";
      description = lib.mdDoc ''
        Base dir to execute drush commands in. This should be the drupal root dir.
      '';
    };

    drushCommand = mkOption {
      type = types.str;
      default = "drush";
      description = lib.mdDoc ''
        Path to drush executable.
      '';
    };

    schedulerInterval = mkOption {
      type = types.str;
      default = "60s";
      description = lib.mdDoc ''
        Duration of the scheduler interval. Format must be parsable by time.ParseDuration (https://pkg.go.dev/time#ParseDuration).
      '';
    };

    logLevel = mkOption {
      type = types.enum [ "trace" "debug" "info" ];
      default = "info";
      description = lib.mdDoc ''
        Desired log level.
      '';
    };
  };

  config = mkIf cfg.enable {
    #assertions = [
    #  {
    #    assertion = cfg.sqlDsn == "";
    #    message = "The database DSN must be set in `services.instaQueueScheduler.sqlDsn`";
    #  }
    #];

    systemd.packages = [ cfg.package ];
    systemd.services.instaQueueScheduler = {
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];

      environment = {
        IQS_SQL_DSN = cfg.sqlDsn;
        IQS_DRUSH_BASE_DIR = cfg.drushBaseDir;
        IQS_DRUSH_COMMAND = cfg.drushCommand;
        IQS_SOCKET_PATH = "/run/insta-queue-scheduler/iqs.sock";
        IQS_SCHEDULER_INTERVAL = cfg.schedulerInterval;
        IQS_LOG_LEVEL = cfg.logLevel;
      };
      path = [ cfg.phpCliPackage ];

      serviceConfig = {
        ExecStart = "${cfg.package}/bin/insta_queue_scheduler";
        User = cfg.user;
        Group = cfg.group;
        Restart = "on-failure";
        RestartSec = "5s";
        RuntimeDirectory = "insta-queue-scheduler";
        RuntimeDirectoryMode = "0755";
        ProtectSystem = "strict";
        ProtectHome = true;
        PrivateTmp = true;
        PrivateDevices = true;
        PrivateUsers = true;
        ProtectClock = true;
        ProtectHostname = true;
        ProtectKernelLogs = true;
        ProtectKernelModules = true;
        ProtectKernelTunables = true;
        ProtectControlGroups = true;
        RestrictNamespaces = true;
        LockPersonality = true;
        RestrictRealtime = true;
        RestrictSUIDSGID = true;
        PrivateMounts = true;
      };
    };

    users.users = optionalAttrs (cfg.user == "iqs") {
      iqs = {
        isSystemUser = true;
        group = cfg.group;
      };
    };

    users.groups = optionalAttrs (cfg.group == "iqs") {
      iqs = {};
    };
  };
}
