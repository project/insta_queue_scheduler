{
  pkgs,
  ...
}: let
  pname = "insta_queue_scheduler";
  version = "git";
in
  pkgs.buildGoModule {
    inherit pname version;

    src = pkgs.lib.cleanSourceWith {
      filter = name: type: let
        baseName = baseNameOf (toString name);
      in
        ! (pkgs.lib.hasSuffix ".nix" baseName);
      src = pkgs.lib.cleanSource ../.;
    };

    vendorHash = "sha256-9/lielWtDA9H19rL/JdVdr3dKvNEOfuDnyTJ7XC9lTw=";

    CGO_ENABLED = 0;
  }
